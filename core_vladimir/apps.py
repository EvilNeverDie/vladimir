from django.apps import AppConfig


class CoreVladimirConfig(AppConfig):
    name = 'core_vladimir'
