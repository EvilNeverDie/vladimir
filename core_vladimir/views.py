from django.db.models import Q
from rest_framework import status, exceptions
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes

from django.contrib.auth.models import AnonymousUser
from django.db import transaction

from core_vladimir.authentication import ExampleAuthentication
from core_vladimir.models import Place, Coment
from core_vladimir.serializers import UserCreateSerializer, CustomUserSerializer, UserUpdateSerializer, \
    PlacePOSTSerializer, PlaceGETSerializer, ComentCreateSerializer, ComentGetSerializer, PlacePUTSerializer


class CustomUserViewSet(APIView):
    """
    retrieve:
        Детальное отображаение пользователя
    create:
        Создание пользователя
    list:
        Список пользователей
    """

    def post(self, request, *args, **kwargs):
        serializer = UserCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        inst = serializer.save()
        return Response(status=status.HTTP_201_CREATED, data=CustomUserSerializer(instance=inst).data)

    def put(self, request, *args, **kwargs):
        if isinstance(request.user, AnonymousUser):
            raise exceptions.PermissionDenied()
        serializer = UserUpdateSerializer(instance=request.user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        inst = serializer.save()
        return Response(CustomUserSerializer(instance=inst, context={'request': request}).data)

    def get(self, request, *args, **kwargs):
        if isinstance(request.user, AnonymousUser):
            raise exceptions.PermissionDenied()
        serializer = CustomUserSerializer(instance=request.user, context={'request': request})
        return Response(serializer.data)


@api_view(['GET'])
@authentication_classes((ExampleAuthentication,))
def auth(request):
    if isinstance(request.user, AnonymousUser):
        raise exceptions.PermissionDenied()
    serializer = CustomUserSerializer(instance=request.user, context={'request': request})
    return Response(serializer.data)


@api_view(['PATCH'])
@permission_classes([IsAuthenticated])
def like_place(request, pk):
    place = Place.objects.get(id=pk)
    context = {}
    if request.user:
        context['user'] = request.user
    if request.query_params.get('delete') and (request.user in list(place.who_like.all())):
        place.likes -= 1
        place.who_like.remove(request.user)
        place.save()
        return Response(PlaceGETSerializer(place, context=context).data)

    if request.user in list(place.who_like.all()):
        raise APIException('Уже лайкнули')
    place.likes += 1
    place.who_like.add(request.user)
    place.save()
    return Response(PlaceGETSerializer(place, context=context).data)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def post_place(request):
    context = {}
    if request.user:
        context['user'] = request.user
    serializer = PlacePOSTSerializer(data=request.data, context={'user': request.user})
    serializer.is_valid(raise_exception=True)
    place = serializer.save()
    return Response(status=status.HTTP_201_CREATED,
                    data=PlaceGETSerializer(place, context=context).data)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def put_place(request, pk):
    place = Place.objects.get(id=pk)
    serializer = PlacePUTSerializer(instance=place, data=request.data)
    serializer.is_valid(raise_exception=True)
    place = serializer.save()
    return Response(PlaceGETSerializer(place, context={}).data)


@api_view(['GET'])
def get_places(request):
    context = {}
    if request.user:
        context['user'] = request.user

    if request.query_params.get('state_id'):
       # places = Place.objects.filter(user_id=request.user.id)
        #places1 = places.filter(state_id = 2)
        places = Place.objects.filter(Q(user=request.user) & Q(state_id=request.query_params.get('state_id')))
        serializer = PlaceGETSerializer(places, many=True, context=context)
        return Response(serializer.data)

    if request.query_params.get('string_in'):
        string_in = request.query_params.get('string_in')
        places = Place.objects.all()
        in_list = []
        for place in list(places):
            if string_in in str(place.name):
                in_list.append(place.id)
        result = Place.objects.filter(id__in=in_list)
        serializer = PlaceGETSerializer(result, many=True, context=context)
        return Response(serializer.data)
    if request.query_params.get('user_id'):
        place = Place.objects.filter(user_id=request.query_params.get('user_id'))
        serializer = PlaceGETSerializer(place, many=True)
        return Response(serializer.data)
    if request.query_params.get('place_id'):
        place = Place.objects.get(id = request.query_params.get('place_id'))
        serializer = PlaceGETSerializer(place, context=context)
        return Response(serializer.data)

    places = Place.objects.filter(state_id=2)
    serializer = PlaceGETSerializer(places, many=True, context=context)
    return Response(serializer.data)


@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticated])
def coment(request, pk):
    if request.method == 'POST':
        serializer = ComentCreateSerializer(data=request.data, context={'user': request.user,
                                                                        'place_id': pk})
        serializer.is_valid(raise_exception=True)
        inst = serializer.save()
        coments = Coment.objects.filter(place_id=pk).order_by('date')
        return Response(ComentGetSerializer(coments, many=True).data)

    if request.method == 'GET':
        coments = Coment.objects.filter(place_id=pk).order_by('date')
        return Response(ComentGetSerializer(coments, many=True).data)
