from rest_framework import serializers
from rest_framework.exceptions import ParseError, APIException

from django.contrib.auth.hashers import make_password
from django.db.utils import IntegrityError
from datetime import datetime
from core_vladimir.models import CustomUser, Place, State, Coment


# TODO 1    картинка, , одобрено
#


class PlaceGETSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    liked = serializers.SerializerMethodField()
    place_pic = serializers.SerializerMethodField()
    author = serializers.SerializerMethodField()
    class Meta:
        model = Place
        fields = ['id', 'name', 'history', 'likes', 'status', 'place_pic', 'address', 'x_coord', 'y_coord', 'user',
                  'liked', 'author']

    def get_author(self, obj):
        user = CustomUser.objects.get(id = obj.user_id)
        return f'{user.first_name} {user.last_name}'

    def get_place_pic(self, obj):
        return 'https://mighty-beyond-83172.herokuapp.com/media/' + str(obj.place_pic)

    def get_status(self, obj):
        status = State.objects.get(id=obj.state_id)
        return status.status_name

    def get_liked(self, obj):
        if self.context.get('user'):
            user = self.context.get('user')
            if user in list(obj.who_like.all()):
                return True
            else:
                return False
        else:
            return False


class ComentGetSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    date = serializers.SerializerMethodField()
    class Meta:
        model = Coment
        fields = ['text', 'date', 'place', 'user']

    def get_user(self, obj):
        user = CustomUser.objects.get(id= obj.user_id)
        return f'{user.first_name} {user.last_name}'

    def get_date(self, obj):
        return str(obj.date).split('.')[0]

class ComentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coment
        fields = ['text']

    def create(self, validated_data):
        user = self.context.get('user')
        place = self.context.get('place_id')
        coment = Coment(**validated_data)
        coment.save()
        coment.user_id = user.id
        coment.date = datetime.now()
        coment.place_id = place
        coment.save()
        return coment


class PlacePUTSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = ['place_pic']


class PlacePOSTSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        exclude = ['likes', 'state', 'who_like']

    def create(self, validated_data):
        user = self.context.get('user')
        place = Place(**validated_data)
        place.save()
        place.user_id = user.id
        place.state_id = 1
        place.save()
        return place


class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = '__all__'




class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'email', 'password')

    def create(self, validated_data):
        fields = set(self.Meta.fields) - set(validated_data.keys())
        if fields:
            raise ParseError('Отсутствуют поля - ' + str(fields))
        validated_data['password'] = make_password(validated_data['password'])
        validated_data.update({'username': validated_data['email']})
        inst = CustomUser(**validated_data)
        try:
            inst.save()
        except IntegrityError:
            raise APIException('Такой пользователь уже существует!')
        return inst


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name')

