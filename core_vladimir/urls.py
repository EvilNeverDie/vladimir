from django.urls import path

from core_vladimir.views import CustomUserViewSet, post_place, get_places, like_place, coment, put_place

urlpatterns = [
    path('users/', CustomUserViewSet.as_view()),
    path('place_post/', post_place),
    path('place_get/', get_places),
    path('place_like/<int:pk>/', like_place),
    path('place_put/<int:pk>/', put_place),
    path('comment/<int:pk>/', coment)
]
