from django.db import models
from django.core.validators import validate_email
from django.contrib.auth.models import AbstractUser, AnonymousUser


class CustomUser(AbstractUser):


    def save(self, *args, **kwargs):
        validate_email(self.email)
        self.username = self.email
        super(CustomUser, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return f'{self.id} - {self.username}'






class State(models.Model):
    status_name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.id} - {self.status_name}'




class Place(models.Model):
    place_pic = models.ImageField(verbose_name='place pic', default=None,
                                 blank=True, null=True)
    name = models.CharField(max_length=100)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
    address = models.CharField(max_length=200)
    state = models.ForeignKey(State, on_delete=models.CASCADE, null=True)
    history = models.TextField()
    x_coord = models.FloatField()
    y_coord = models.FloatField()
    likes = models.IntegerField(default=0)
    who_like = models.ManyToManyField(CustomUser, related_name='places2users', null=True)

    def __str__(self):
        return f'{self.id} - {self.name}'

class Coment(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
    date = models.DateTimeField(null=True)
    text = models.TextField()
    place = models.ForeignKey(Place, on_delete=models.CASCADE, null=True)
