from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from core_vladimir.models import CustomUser, Place, State


@admin.register(CustomUser)
class UserAdmin(admin.ModelAdmin):
    pass

@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    pass

@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    pass