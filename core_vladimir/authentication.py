
from rest_framework import authentication
from rest_framework import exceptions

from django.contrib.auth import authenticate

from core_vladimir.models import CustomUser


class ExampleAuthentication(authentication.BasicAuthentication):
    def authenticate_credentials(self, userid, password, request=None):
        try:
            CustomUser.objects.get(username=userid)  # get the user
        except CustomUser.DoesNotExist:
            raise exceptions.NotFound('No such user')  # raise exception if user does not exist
        credentials = {
            'username': userid,
            'password': password
        }
        user = authenticate(request=request, **credentials)
        if user:
            return (user, None)  # authentication successful
        else:
            raise exceptions.AuthenticationFailed('Wrong Password')
